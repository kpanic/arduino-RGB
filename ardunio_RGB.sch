EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:ardunio_RGB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ardunio RGB"
Date "2016-02-14"
Rev "1.0"
Comp "Рога и Копыта"
Comment1 ""
Comment2 "Раб"
Comment3 "Рабовладелец"
Comment4 ""
$EndDescr
$Comp
L CONN_01X04 P2
U 1 1 56C09EAF
P 1700 2200
F 0 "P2" H 1700 2450 50  0000 C CNN
F 1 "CONN_01X04" V 1800 2200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1700 2200 50  0001 C CNN
F 3 "" H 1700 2200 50  0000 C CNN
	1    1700 2200
	-1   0    0    1   
$EndComp
Wire Wire Line
	2900 2750 2900 2250
Wire Wire Line
	2900 1500 2900 2050
$Comp
L GND #PWR01
U 1 1 56C0A711
P 2050 2700
F 0 "#PWR01" H 2050 2450 50  0001 C CNN
F 1 "GND" H 2050 2550 50  0000 C CNN
F 2 "" H 2050 2700 50  0000 C CNN
F 3 "" H 2050 2700 50  0000 C CNN
	1    2050 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	1700 2350 3200 2350
$Comp
L CONN_01X04 P1
U 1 1 56C0A748
P 1700 1300
F 0 "P1" H 1700 1550 50  0000 C CNN
F 1 "CONN_01X04" V 1800 1300 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 1700 1300 50  0001 C CNN
F 3 "" H 1700 1300 50  0000 C CNN
	1    1700 1300
	-1   0    0    1   
$EndComp
Wire Wire Line
	3200 2500 4500 2500
Wire Wire Line
	4500 2500 4500 750 
Wire Wire Line
	4500 750  2100 750 
Wire Wire Line
	3200 1250 3200 1200
$Comp
L +12V #PWR02
U 1 1 56C0A9EF
P 2300 1750
F 0 "#PWR02" H 2300 1600 50  0001 C CNN
F 1 "+12V" H 2300 1890 50  0000 C CNN
F 2 "" H 2300 1750 50  0000 C CNN
F 3 "" H 2300 1750 50  0000 C CNN
	1    2300 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 1450 2300 3000
Wire Wire Line
	3200 1650 3200 1750
Wire Wire Line
	3200 1750 2700 1750
Wire Wire Line
	2700 1750 2700 2900
Wire Wire Line
	3200 2350 3200 2300
Connection ~ 2700 2350
Wire Wire Line
	2700 2900 3200 2900
$Comp
L CONN_01X02 P3
U 1 1 56C0AD95
P 2250 3200
F 0 "P3" H 2250 3350 50  0000 C CNN
F 1 "CONN_01X02" V 2350 3200 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x02" H 2250 3200 50  0001 C CNN
F 3 "" H 2250 3200 50  0000 C CNN
	1    2250 3200
	0    1    1    0   
$EndComp
Connection ~ 2300 1750
Wire Wire Line
	1900 2650 2200 2650
Wire Wire Line
	2200 2650 2200 3000
Connection ~ 2050 2650
Wire Wire Line
	3200 1200 1900 1200
Wire Wire Line
	1900 1200 1900 1150
Wire Wire Line
	3200 1900 2800 1900
Wire Wire Line
	2800 1900 2800 1250
Wire Wire Line
	2800 1250 1900 1250
Wire Wire Line
	2100 750  2100 1350
Wire Wire Line
	2100 1350 1900 1350
Wire Wire Line
	1900 1450 2300 1450
Wire Wire Line
	2900 2050 1900 2050
Wire Wire Line
	2900 2150 1900 2150
Wire Wire Line
	2900 2250 1900 2250
Wire Wire Line
	1900 2350 1900 2650
Connection ~ 1900 2350
$Comp
L IRLZ34N Q1
U 1 1 56C0D840
P 3100 1450
F 0 "Q1" H 3300 1525 50  0000 L CNN
F 1 "IRLZ34N" H 3300 1450 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 3300 1375 50  0000 L CIN
F 3 "" H 3100 1450 50  0000 L CNN
	1    3100 1450
	1    0    0    -1  
$EndComp
$Comp
L IRLZ34N Q2
U 1 1 56C0D899
P 3100 2100
F 0 "Q2" H 3300 2175 50  0000 L CNN
F 1 "IRLZ34N" H 3300 2100 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 3300 2025 50  0000 L CIN
F 3 "" H 3100 2100 50  0000 L CNN
	1    3100 2100
	1    0    0    -1  
$EndComp
$Comp
L IRLZ34N Q3
U 1 1 56C0D9F9
P 3100 2700
F 0 "Q3" H 3300 2775 50  0000 L CNN
F 1 "IRLZ34N" H 3300 2700 50  0000 L CNN
F 2 "Power_Integrations:TO-220" H 3300 2625 50  0000 L CIN
F 3 "" H 3100 2700 50  0000 L CNN
	1    3100 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 2700 2050 2650
$EndSCHEMATC
